module.exports = function( grunt ) {

	'use strict';

	// Project configuration
	grunt.initConfig({

		pkg: grunt.file.readJSON( 'package.json' ),

		addtextdomain: {
			options: {
				textdomain: 'ethpress_token_roles'
			},
			update_all_domains: {
				options: {
					updateDomains: true
				},
				src: [ '*.php', '**/*.php', '!\.git/**/*', '!bin/**/*', '!node_modules/**/*', '!tests/**/*' ]
			}
		},

		wp_readme_to_markdown: {
			your_target: {
				files: {
					'README.md': 'readme.txt'
				}
			}
		},

		makepot: {
			target: {
				options: {
					domainPath: '/languages',
					exclude: [ '\.git/*', 'bin/*', 'node_modules/*', 'tests/*' ],
					mainFile: 'ethpress-token-roles.php',
					potFilename: 'ethpress-token-roles.pot',
					potHeaders: {
						poedit: true,
						'x-poedit-keywordslist': true
					},
					type: 'wp-plugin',
					updateTimestamp: true
				}
			}
		},

		compress: {
			main: {
				options: {
					archive: 'ethpress-token-roles.zip'
				},
				files: [
					{
						src: [ 'public/**/*', 'vendor/**/*', 'app/**/*', 'languages/**/*', 'ethpress-token-roles.php', 'readme.txt', 'index.php', 'LICENSE' ],
						dest: './'
					}
				]
			}
		}
	});

	grunt.loadNpmTasks( 'grunt-wp-i18n' );
	grunt.loadNpmTasks( 'grunt-wp-readme-to-markdown' );
	grunt.loadNpmTasks( 'grunt-contrib-compress' );
	grunt.registerTask( 'default', [ 'i18n', 'readme' ]);
	grunt.registerTask( 'i18n', [ 'addtextdomain', 'makepot' ]);
	grunt.registerTask( 'readme', [ 'wp_readme_to_markdown' ]);
	grunt.registerTask( 'pack', [ 'compress' ]);

	grunt.util.linefeed = '\n';

};
